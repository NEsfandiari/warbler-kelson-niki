from flask import redirect, render_template, request, url_for, Blueprint, flash
from project.messages.models import Message
from project.users.views import ensure_correct_user
from project.users.models import User
from project.messages.forms import MessageForm, LikeForm
from flask_login import current_user, login_required
from project import db
from functools import wraps

messages_blueprint = Blueprint(
    'messages', __name__, template_folder='templates')


def ensure_correct_user(fn):
    @wraps(fn)
    def wrapper(*args, **kwargs):
        if kwargs.get('id') != current_user.id:
            flash({'text': "Not Authorized", 'status': 'danger'})
            return redirect(url_for('root'))
        return fn(*args, **kwargs)

    return wrapper


@messages_blueprint.route('/', methods=["POST"])
@login_required
def index(id):
    if current_user.get_id() == str(id):
        form = MessageForm()
        from IPython import embed
        embed()
        if form.validate():
            new_message = Message(text=form.text.data, user_id=id)
            db.session.add(new_message)
            db.session.commit()
            return redirect(url_for('users.show', id=id))
    return render_template('messages/new.html', form=form)


@messages_blueprint.route('/new')
@login_required
@ensure_correct_user
def new(id):
    return render_template('messages/new.html', form=MessageForm())


@messages_blueprint.route('/<int:message_id>', methods=["GET", "DELETE"])
def show(id, message_id):
    found_message = Message.query.get(message_id)
    if request.method == b"DELETE" and current_user.id == id:
        db.session.delete(found_message)
        db.session.commit()
        return redirect(url_for('users.show', id=id))
    return render_template(
        'messages/show.html', message=found_message, form=LikeForm())


@messages_blueprint.route('/<int:message_id>/like', methods=['POST', 'DELETE'])
@login_required
def like_toggle(id, message_id):
    user = current_user
    form = LikeForm(request.form)
    if request.method == 'POST':
        user.message_likes.append(Message.query.get_or_404(message_id))
        db.session.commit()
        return redirect(url_for('root', id=id, message_id=message_id))
    if request.method == b'DELETE':
        user.message_likes.remove(Message.query.get_or_404(message_id))
        db.session.commit()
        return redirect(url_for('root', id=id, message_id=message_id))
    return redirect(url_for('root'))