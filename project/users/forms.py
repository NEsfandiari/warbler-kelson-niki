from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField
from wtforms.validators import DataRequired, Email, Length
from wtforms.widgets import TextArea


class UserForm(FlaskForm):
    username = StringField('username', validators=[DataRequired()])
    display_name = StringField('display_name', validators=[Length(max=35)])
    bio = StringField('bio', validators=[Length(max=300)], widget=TextArea())
    location = StringField('location', validators=[Length(max=100)])
    email = StringField('email', validators=[DataRequired(), Email()])
    image_url = StringField('image_url')
    header_image_url = StringField('header_image_url')
    password = PasswordField('password', validators=[Length(min=6)])


class LoginForm(FlaskForm):
    username = StringField('username', validators=[DataRequired()])
    password = PasswordField('password', validators=[Length(min=6)])
